const fs = require('fs');
const path = require('path')

function filesWithExt(files, fileExt) {
  return files.filter(function(file) {
    return path.extname(file) === '.' + fileExt;
  });
};

module.exports = function(directory, fileExt, callback) {
  fs.readdir(directory, function(err, files) {
    if(err) return callback(err);
    return callback(null, filesWithExt(files, fileExt));
  });
};