const myModule = require('./mymodule');

const directory = process.argv[2];
const fileExt = process.argv[3];

const callback = function(err, filtered) {
  if(err) throw(err);
  for(let file in filtered) {
    console.log(filtered[file]);
  };
};

myModule(directory, fileExt, callback);