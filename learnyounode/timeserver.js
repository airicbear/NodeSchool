const net = require('net');

function zeroFill(num) {
  return (num < 10 ? '0' : '') + num;
}

function now() {
  const d = new Date();
  return d.getFullYear() + '-' +
    zeroFill(d.getMonth() + 1) + '-' +
    zeroFill(d.getDate()) + ' ' +
    zeroFill(d.getHours()) + ':' +
    zeroFill(d.getMinutes());
}

const server = net.createServer(function(socket) {
  socket.end(now() + '\n');
});

server.listen(process.argv[2]);