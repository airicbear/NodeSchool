const fs = require('fs');
const path = require('path');

fs.readdir(process.argv[2], function(err, data){
  if(err) throw err;
  let textFiles = data.toString().split(',');
  textFiles.forEach(function(element) {
    if('.' + process.argv[3] === path.extname(element)) {
      console.log(element);
    }
  });
});